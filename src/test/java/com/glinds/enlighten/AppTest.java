package com.glinds.enlighten;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * happy path from requirements
 * feed in less than alert triggers for each type, 
 * feed in another type other than TSTAT or BATT
 * feed in some more than 5min ago, both types
 * feed in multiple satelite types
 * check alert type is right spelling
 * check timestamp format -> iso
 */
public class AppTest 
{
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private final String DEFAULT_NO_ALERT_MSG = "INFO: No alerts in this data process";

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    
    @Test
    public void testAppMain_HappyPath(){
        String expectedOutput = getStringFromFile("output/happy_path-red_high_red_low.json");

        App.main(null);

        assertEquals(expectedOutput, outContent.toString().trim());
    }

    @Test
    public void testAppMain_NoAlert_NoRedHighBreach(){
        App.main(new String[]{"input/test_satelite_data-red_high_but_no_breach.txt"});

        assertEquals(DEFAULT_NO_ALERT_MSG, outContent.toString().trim());
    }

    @Test
    public void testAppMain_NoAlert_NoRedLowBreach(){
        App.main(new String[]{"input/test_satelite_data-red_low_but_no_breach.txt"});

        assertEquals(DEFAULT_NO_ALERT_MSG, outContent.toString().trim());
    }

    @Test
    public void testAppMain_NoAlert_TriggersButMoreThan5Min(){
        App.main(new String[]{"input/test_satelite_data-more_than_5_min_ago.txt"});

        assertEquals(DEFAULT_NO_ALERT_MSG, outContent.toString().trim());
    }

    @Test
    public void testAppMain_OneAlert_RedHigh(){
        String expectedOutput = getStringFromFile("output/one-alert-red-high.json");

        App.main(new String[]{"input/test_satelite_data-red_high_only_alert.txt"});

        assertEquals(expectedOutput, outContent.toString().trim());
    }

    @Test
    public void testAppMain_OneAlert_RedLow(){
        String expectedOutput = getStringFromFile("output/one-alert-red-low.json");

        App.main(new String[]{"input/test_satelite_data-red_low_only_alert.txt"});

        assertEquals(expectedOutput, outContent.toString().trim());
    }

    private String getStringFromFile(String sourceFileName) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classloader.getResourceAsStream(sourceFileName);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        BufferedReader br = new BufferedReader(inputStreamReader);
        return br.lines().collect(Collectors.joining(System.lineSeparator()));
    }
}

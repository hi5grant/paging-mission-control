package com.glinds.enlighten;

import lombok.Data;

@Data
public class AlertMessage {
    private int satelliteId;
    private String severity;
    private String component;
    private String timestamp;

    public AlertMessage(SateliteEntry sateliteEntry, String severity) {
        this.satelliteId = sateliteEntry.getSateliteId();
        this.component = sateliteEntry.getComponent();
        this.timestamp = sateliteEntry.getTimestamp().toString();
        this.severity = severity;
    }
}

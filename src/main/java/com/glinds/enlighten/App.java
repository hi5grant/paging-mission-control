package com.glinds.enlighten;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final String DEFAULT_RESOURCES_FILENAME = "satelite_data.txt";
    private static final int BATTERY_ALERT_THRESHOLD = 3;
    private static final int THERMOSTAT_ALERT_THRESHOLD = 3;
    private static final int ALERT_SECONDS_INTERVAL_THRESHOLD = 5 * 60; // 5 minutes

    public static void main( String[] args )
    {
        // allow resources filename to be configurable
        String sourceFileName = DEFAULT_RESOURCES_FILENAME;
        if (args != null && args.length > 0) {
            sourceFileName = args[0];
        }
        BufferedReader br = retrieveInputBufferedReader(sourceFileName);

        List<SateliteEntry> thermalTriggers = new ArrayList<>();
        List<SateliteEntry> batteryTriggers = new ArrayList<>();
        List<AlertMessage> alertMessages = new ArrayList<>();

        br.lines().forEach(line -> {
            SateliteEntry sateliteEntry = parseSateliteEntry(line);

            if ("TSTAT".equals(sateliteEntry.getComponent()) 
                && sateliteEntry.getRawValue() > sateliteEntry.getRedHighLimit()
            ) {
                scanTriggerForThermalAlert(sateliteEntry, thermalTriggers, alertMessages);
            }
            if ("BATT".equals(sateliteEntry.getComponent()) 
                && sateliteEntry.getRawValue() < sateliteEntry.getRedLowLimit()
            ) {
                scanTriggerForBatteryAlert(sateliteEntry, batteryTriggers, alertMessages);
            }
        });

        if (alertMessages.size() > 0) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            // TODO - make ISO timestamp
            String serializedAlertMessages = gson.toJson(alertMessages);
            System.out.println(serializedAlertMessages);    
        } else {
            System.out.println("INFO: No alerts in this data process");
        }
    }

    private static BufferedReader retrieveInputBufferedReader(String fileName) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classloader.getResourceAsStream(fileName);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        return new BufferedReader(inputStreamReader);
    }

    static SateliteEntry parseSateliteEntry(String line) {
        String[] parsedEntry = line.split(Pattern.quote("|"));
        return new SateliteEntry(parsedEntry);
    }

    static void scanTriggerForThermalAlert(SateliteEntry sateliteEntry, List<SateliteEntry> triggers, List<AlertMessage> alerts) {
        scanTriggerForAlert(sateliteEntry, triggers, alerts, AlertType.RED_HIGH);
    }

    static void scanTriggerForBatteryAlert(SateliteEntry sateliteEntry, List<SateliteEntry> triggers, List<AlertMessage> alerts) {
        scanTriggerForAlert(sateliteEntry, triggers, alerts, AlertType.RED_LOW);
    }

    static int getAlertThreshold(AlertType alertType) {
        switch (alertType) {
            case RED_HIGH:
                return THERMOSTAT_ALERT_THRESHOLD;
            case RED_LOW:
                return BATTERY_ALERT_THRESHOLD;
            default:
                return 0;
        }
    }

    static void scanTriggerForAlert(SateliteEntry sateliteEntry, List<SateliteEntry> triggers, List<AlertMessage> alerts, AlertType alertType) {
        triggers.add(sateliteEntry);
        int sateliteRedTriggerCounter = 0;
        for (int i = 0; i < triggers.size(); i++) { // can't foreach if I need to increment an outside-of-lambda variable
            SateliteEntry trigger = triggers.get(i);
            if (trigger.getSateliteId() == sateliteEntry.getSateliteId()
                && trigger.getTimestamp().isAfter(sateliteEntry.getTimestamp().minusSeconds(ALERT_SECONDS_INTERVAL_THRESHOLD))  
            ) {
                sateliteRedTriggerCounter++;
                if (sateliteRedTriggerCounter >= getAlertThreshold(alertType)) {
                    alerts.add(new AlertMessage(sateliteEntry, alertType.getAlertTypeMessage()));
                    break; // for speed
                }
            }
        }
    }
}

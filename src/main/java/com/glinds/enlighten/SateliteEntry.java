package com.glinds.enlighten;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import lombok.Data;

@Data
public class SateliteEntry {
    private Instant timestamp;
    private int sateliteId;
    private int redHighLimit;
    private int yellowHighLimit;
    private int redLowLimit;
    private int yellowLowLimit;
    private double rawValue;
    private String component;
    
    public SateliteEntry(String[] entryRow) {
        this.timestamp = LocalDateTime
                            .parse(entryRow[0], 
                                DateTimeFormatter
                                    .ofPattern("yyyyMMdd HH:mm:ss.SSS"))
                            .toInstant(ZoneOffset.UTC);
        this.sateliteId = Integer.parseInt(entryRow[1]);
        this.redHighLimit = Integer.parseInt(entryRow[2]);
        this.yellowHighLimit = Integer.parseInt(entryRow[3]);
        this.redLowLimit = Integer.parseInt(entryRow[4]);
        this.yellowLowLimit = Integer.parseInt(entryRow[5]);
        this.rawValue = Double.parseDouble(entryRow[6]);
        this.component = entryRow[7];
    }
}

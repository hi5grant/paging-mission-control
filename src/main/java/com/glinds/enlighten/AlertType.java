package com.glinds.enlighten;

public enum AlertType {
    RED_HIGH ("RED HIGH"), 
    RED_LOW ("RED LOW");

    private String alertTypeMessage;

    private AlertType(String alertTypeMessage) {
        this.alertTypeMessage = alertTypeMessage; 
    }
 
    public String getAlertTypeMessage() {
        return alertTypeMessage;
    }
}
